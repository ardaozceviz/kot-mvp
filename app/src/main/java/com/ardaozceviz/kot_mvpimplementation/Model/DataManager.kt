package com.ardaozceviz.kot_mvpimplementation.Model

/**
 * Created by arda on 01/10/2017.
 */

class DataManager(internal var mSharedPrefsHelper: SharedPrefsHelper) {

    val emailId: String?
        get() = mSharedPrefsHelper.email

    val loggedInMode: Boolean?
        get() = mSharedPrefsHelper.loggedInMode

    fun clear() {
        mSharedPrefsHelper.clear()
    }

    fun saveEmailId(email: String) {
        mSharedPrefsHelper.putEmail(email)
    }

    fun setLoggedIn() {
        mSharedPrefsHelper.loggedInMode = true
    }
}