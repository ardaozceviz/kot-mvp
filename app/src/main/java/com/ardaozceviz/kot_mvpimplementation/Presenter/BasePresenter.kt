package com.ardaozceviz.kot_mvpimplementation.Presenter

import com.ardaozceviz.kot_mvpimplementation.Model.DataManager
import com.ardaozceviz.kot_mvpimplementation.View.MvpView

/**
 * Created by arda on 01/10/2017.
 */

class BasePresenter<V : MvpView>(dataManager: DataManager) : MvpPresenter<V> {

    var mvpView: V? = null
        private set

    var dataManager: DataManager
        internal set


    init {
        this.dataManager = dataManager
    }

    override fun onAttach(mvpView: V) {
        this.mvpView = mvpView
    }
}