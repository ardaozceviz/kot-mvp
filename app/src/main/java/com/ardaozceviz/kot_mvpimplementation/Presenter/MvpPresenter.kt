package com.ardaozceviz.kot_mvpimplementation.Presenter

import com.ardaozceviz.kot_mvpimplementation.View.MvpView

/**
 * Created by arda on 01/10/2017.
 */

interface MvpPresenter<V : MvpView> {
    fun onAttach(mvpView: V)
}