package com.ardaozceviz.kot_mvpimplementation.View

import android.support.v7.app.AppCompatActivity

/**
 * Created by arda on 01/10/2017.
 */
// BaseActivity extends AppCompatActivity implements MvpView
class BaseActivity : AppCompatActivity(), MvpView